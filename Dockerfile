FROM node:20-alpine as build

WORKDIR /app

COPY ./package.json ./yarn.lock ./

RUN yarn install

COPY . .

RUN yarn build

RUN rm -r ./node_modules

RUN yarn install --prod

FROM node:20-alpine as production

WORKDIR /app

COPY --from=build /app/node_modules ./node_modules

COPY --from=build /app/dist ./dist

COPY .env ./

EXPOSE 3100

CMD ["node", "dist/index.js"]