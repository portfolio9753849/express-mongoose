import 'reflect-metadata';
import * as express from 'express';

import bookRoutes from './routes/book';

const app: express.Express = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/books', bookRoutes);

export default app;