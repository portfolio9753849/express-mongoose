import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { Request, Response, NextFunction } from 'express';

export function requestValidator(dto: any) {
  return async (req: Request, res: Response, next: NextFunction) => {
    let requestData: any;
    switch (req.method) {
      case 'GET':
        requestData = req.query;
        break;
      case 'POST':
      case 'PUT':
      case 'PATCH':
        requestData = req.body;
        break;
    }
    const errors = await validate(plainToInstance(dto, requestData))

    if (errors.length > 0) {
      return res.status(400).send(errors);
    }
    next();
  }
} 