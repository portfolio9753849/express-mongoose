import { Schema, model } from "mongoose";

export interface IBook {
  title: string;
  author: string;
  publisher: string;
  publishingDate: Date;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
}

const bookSchema = new Schema<IBook>({
  title: {
    type: String,
    required: true,
  },
  author: {
    type: String,
    required: true,
  },
  publisher: {
    type: String,
    required: true,
  },
  publishingDate: Date,
  createdAt: {
    type: Date,
    immutable: true,
    default: () => Date.now(),
  },
  updatedAt: {
    type: Date,
    default: null
  },
  deletedAt: {
    type: Date,
    default: null
  }
});

const Book = model('Book', bookSchema);
export default Book;