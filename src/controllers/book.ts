import { Request, Response } from 'express';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';

import { BookData, BookFilters, BookUpdateData } from '../dtos/books';
import BookService from '../services/book';

class BookController {
  async findAll(req: Request, res: Response): Promise<Response> {
    const books = await BookService.findAll(req.query as any);
    return res.json(books);
  }

  async findById(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;
    const book = await BookService.findById(id);
    return res.json(book);
  }

  async create(req: Request, res: Response): Promise<Response> {
    const book = await BookService.create(req.body);
    return res.json(book);
  }

  async update(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;

    const book = await BookService.findById(id);

    if (!book) {
      return res.status(404).send({ message: 'Book not found'});
    }

    await BookService.update(id, req.body);
    return res.status(204).send();
  }

  async delete(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;

    const book = await BookService.findById(id);

    if (!book) {
      return res.status(404).send({ message: 'Book not found'});
    }

    await BookService.delete(id);
    return res.status(204).send();
  }
}

export default new BookController();