import Book from "../models/book";

import { BookData, BookFilters, BookResponse, BookUpdateData } from "src/dtos/books";

class BookService {
  async findAll(bookFilters: BookFilters): Promise<BookResponse[]> {
    const { page, size } = bookFilters;

    let filters: any = {};
    if (bookFilters.title) {
      filters.title = new RegExp(bookFilters.title, 'i');
    }
    if (bookFilters.author) {
      filters.author = new RegExp(bookFilters.author, 'i');
    }
    if (bookFilters.publisher) {
      filters.publisher = new RegExp(bookFilters.publisher, 'i');
    }
    if (bookFilters.publishingDate) {
      filters.publishingDate = bookFilters.publishingDate;
    }

    const books = await Book.find(filters)
      .select(['_id', 'title', 'author', 'publisher', 'publishingDate', 'createdAt'])
      .skip((page - 1) * size)
      .limit(size);

    return books;
  }

  async findById(id: string): Promise<BookResponse> {
    return await Book.findById(id);
  }

  async create(bookData: BookData): Promise<BookResponse> {
    const book = await Book.create(bookData);
    return book;
  }

  async update(id: string, bookData: BookUpdateData): Promise<void> {
    await Book.updateOne({ _id: id }, bookData);
  }

  async delete(id: string): Promise<void> {
    await Book.deleteOne({ _id: id });
  }
}

export default new BookService();