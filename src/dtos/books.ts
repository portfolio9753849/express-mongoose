import { Type } from "class-transformer";
import { IsDateString, IsNotEmpty, IsOptional, IsNumber, Min } from "class-validator";

export class BookResponse {
  title: string;
  author: string;
  publisher: string;
  publishingDate: Date;
  createdAt: Date;
}

export class BookFilters {
  @IsOptional()
  title: string;

  @IsOptional()
  author: string;

  @IsOptional()
  publisher: string;

  @IsOptional()
  @IsDateString()
  publishingDate: Date;

  @IsNotEmpty()
  @IsNumber()
  @Type(() => Number)
  @Min(1)
  page: number;

  @IsNotEmpty()
  @IsNumber()
  @Type(() => Number)
  @Min(1)
  size: number;
}

export class BookData {

  @IsNotEmpty()
  title: string;

  @IsNotEmpty()
  author: string;

  @IsNotEmpty()
  publisher: string;

  @IsDateString()
  publishingDate: Date;
}

export class BookUpdateData {
  @IsOptional()
  title: string;

  @IsOptional()
  author: string;

  @IsOptional()
  publisher: string;

  @IsOptional()
  publishingDate: Date;
}