import { Router } from 'express';

import bookController from '../controllers/book';
import { BookData, BookFilters, BookUpdateData } from '../dtos/books';
import { requestValidator } from '../middleware/request-validator';

const router = Router();

router.get('/', requestValidator(BookFilters), bookController.findAll);
router.get('/:id', bookController.findById);
router.post('/', requestValidator(BookData), bookController.create);
router.patch('/:id', requestValidator(BookUpdateData), bookController.update);
router.delete('/:id', bookController.delete);

export default router;
