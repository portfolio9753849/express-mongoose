import 'dotenv/config';
import { connect } from 'mongoose';

import app from './app';

async function main() {
  try {
    await connect(process.env.MONGO_URI);
    console.log('MongoDB connected');
  } catch (error) {
    console.log('MongoDB connected');
  }
  app.listen(3100, () => {
    console.log('Server running on port 3100');
  }).on('error', (error: Error) => {
    console.log('Error starting server', error);
  });
}

main();


