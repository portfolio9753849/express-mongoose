## Description

Portfolio project of a REST API built express js that uses MongoDB and Mongoose

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ sudo docker-compose build
$ docker-compose run -d
$ yarn seed:run

# application logs
$ sudo docker-compose logs -f server
```
